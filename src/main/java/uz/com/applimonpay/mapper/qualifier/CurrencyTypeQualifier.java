package uz.com.applimonpay.mapper.qualifier;

import org.mapstruct.Mapper;
import org.mapstruct.Named;
import org.springframework.stereotype.Component;
import uz.com.applimonpay.enums.CurrencyType;

@Component
@Mapper
public class CurrencyTypeQualifier {

    @Named("mapCurrencyType")
    public CurrencyType mapCurrencyType(String currencyTypeName) {
        return CurrencyType.getByName(currencyTypeName);
    }

}
