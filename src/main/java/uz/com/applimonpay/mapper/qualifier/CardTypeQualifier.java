package uz.com.applimonpay.mapper.qualifier;

import org.mapstruct.Mapper;
import org.mapstruct.Named;
import org.springframework.stereotype.Component;
import uz.com.applimonpay.enums.CardType;

@Component
@Mapper
public class CardTypeQualifier {

    @Named("mapCardType")
    public CardType mapCardType(String cardTypeName) {
        return CardType.getByName(cardTypeName);
    }

}
