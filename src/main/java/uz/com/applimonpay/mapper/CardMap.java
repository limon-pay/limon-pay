package uz.com.applimonpay.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import uz.com.applimonpay.dto.card.CardDTO;
import uz.com.applimonpay.dto.card.CardUpDTO;
import uz.com.applimonpay.dto.card.UserCardDTO;
import uz.com.applimonpay.entity.Card;
import uz.com.applimonpay.mapper.qualifier.CardTypeQualifier;
import uz.com.applimonpay.mapper.qualifier.CurrencyTypeQualifier;

@Mapper(componentModel = "spring",
        uses = {
                UserMap.class,
                CurrencyTypeQualifier.class,
                CardTypeQualifier.class
        }
)
public interface CardMap {

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "active", ignore = true)
    @Mapping(target = "uuid", ignore = true)
    @Mapping(target = "phone", source = "phone")
    Card toEntity(@MappingTarget Card card, CardUpDTO dto);

    @Mapping(target = "id", source = "id")
    @Mapping(target = "uuid", source = "uuid")
    @Mapping(target = "active", source = "active")
    @Mapping(target = "deleted", source = "deleted")
    @Mapping(target = "createdAt", source = "createdAt")
    @Mapping(target = "updatedAt", source = "updatedAt")
    @Mapping(target = "pan", source = "pan")
    @Mapping(target = "expiry", source = "expiry")
    @Mapping(target = "maskedPan", source = "maskedPan")
    @Mapping(target = "cardHolderName", source = "cardHolderName")
    @Mapping(target = "type", source = "type")
    @Mapping(target = "currency", source = "currency")
    @Mapping(target = "phone", source = "phone")
    @Mapping(target = "balance", source = "balance")
    @Mapping(target = "user", source = "user")
    @Mapping(target = "cardToken", source = "cardToken")
    CardDTO toDto(Card card);


    @Mapping(target = "pan", source = "pan")
    @Mapping(target = "expiry", source = "expiry")
    @Mapping(target = "type", source = "type")
    @Mapping(target = "currency", source = "currency")
    @Mapping(target = "owner", source = "cardHolderName")
    @Mapping(target = "token", source = "cardToken")
    UserCardDTO toEntity(Card card);
}
