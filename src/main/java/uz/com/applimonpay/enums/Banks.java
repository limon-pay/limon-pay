package uz.com.applimonpay.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@Getter
@AllArgsConstructor
public enum Banks {

    ASAKABANK("ASAKA BANK", 'U', 'B'),
    INFINBANK("INFIN BANK", 'U', 'B'),
    IPAKYOLIBANK("IPAK YO'LI BANK", 'U', 'B'),
    MIKROKREDITBANK("MIKROKREDIT BANK", 'U', 'B'),
    ORIENTFINANSBANK("ORIENT FINANS BANK", 'U', 'B'),
    TRANSBANK("TRANS BANK", 'U', 'B'),
    ZIRAATBANK("ZIRAAT BANK", 'U', 'B'),
    IPOTEKA("IPOTEKA", 'U', 'B'),
    QISHLOQQURILISHBANK("QISHLOQ QURILISH BANK", 'U', 'B'),
    ALOQABANK("ALOQA BANK", 'U', 'B'),
    XALQBANK("XALQ BANK", 'U', 'B'),
    KAPITALBANK("KAPITAL BANK", 'U', 'B'),
    TURONBANK("TURON BANK", 'U', 'B'),
    AGROBANK("AGRO BANK", 'U', 'B'),
    UNKNOWN("UNKNOWN", 'U', 'B');


    private final String descr;
    private final Character anotherBank;
    private final Character isOwnerBank;


    public static Banks getByName(final String name) {
        return Arrays.stream(Banks.values())
                .filter(banks -> banks.name().equalsIgnoreCase(name))
                .findFirst()
                .orElse(UNKNOWN);
    }

}
