package uz.com.applimonpay.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@Getter
@AllArgsConstructor
public enum OperStatus {

    HOLD("HOLD"),
    PENDING("PENDING"),
    SUCCESS("SUCCESS"),
    FAILED("FAILED"),
    REVERSE("REVERSE"),
    UNKNOWN("UNKNOWN");

    private final String description;


    public static OperStatus getByName(final String name) {
        return Arrays.stream(OperStatus.values())
                .filter(operationStatus -> operationStatus.name().equalsIgnoreCase(name))
                .findFirst()
                .orElse(UNKNOWN);
    }


}
