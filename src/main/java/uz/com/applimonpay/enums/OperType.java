package uz.com.applimonpay.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@Getter
@AllArgsConstructor
public enum OperType {

    P2P("CARD TO CARD"),
    P2S("CARD TO SERVICE"),
    UNKNOWN("UNKNOWN");

    private final String description;

    public static OperType getByName(final String name) {
        return Arrays.stream(OperType.values())
                .filter(operationType -> operationType.name().equalsIgnoreCase(name))
                .findFirst()
                .orElse(UNKNOWN);
    }

}
