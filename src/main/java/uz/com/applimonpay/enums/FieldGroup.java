package uz.com.applimonpay.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@Getter
@AllArgsConstructor
public enum FieldGroup {
    CARD("CARD"),
    SERVICE("SERVICE"),
    UNKNOWN("UNKNOWN");

    private final String description;

    public static FieldGroup getByName(final String name) {
        return Arrays.stream(FieldGroup.values())
                .filter(group -> group.name().equalsIgnoreCase(name))
                .findFirst()
                .orElse(UNKNOWN);
    }
}

