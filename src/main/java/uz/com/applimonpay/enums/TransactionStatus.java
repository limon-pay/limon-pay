package uz.com.applimonpay.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@Getter
@AllArgsConstructor
public enum TransactionStatus {

    PENDING("IN PROGRESS"),
    SUCCESS("PROGRESS SUCCESSFUL"),
    ERROR("TRANSACTION ERROR"),
    UNKNOWN("UNKNOWN");

    private final String description;

    public static TransactionStatus getByName(final String name) {
        return Arrays.stream(TransactionStatus.values())
                .filter(transactionStatus -> transactionStatus.name().equalsIgnoreCase(name))
                .findFirst()
                .orElse(UNKNOWN);
    }

}

