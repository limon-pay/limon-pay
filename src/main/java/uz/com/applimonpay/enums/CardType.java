package uz.com.applimonpay.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@Getter
@AllArgsConstructor
public enum CardType {
    UZCARD("uzcard", "U", "N", 8600),
    HUMO("humo", "H", "N", 9860),
    VISA("visa", "V", "N", 4000),
    UNKNOWN("unknown", "U", "U", 0);

    private final String name;
    private final String initial;
    private final String isNational;
    private final Integer code;

    public static CardType getByName(final String name) {
        return Arrays.stream(CardType.values())
                .filter(card -> card.name().equalsIgnoreCase(name))
                .findFirst()
                .orElse(UNKNOWN);
    }
}
