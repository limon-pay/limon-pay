package uz.com.applimonpay.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@Getter
@AllArgsConstructor
public enum TransactionType {

    DEBIT("INCOME"),
    CREDIT("OUTCOME"),
    UNKNOWN("UNKNOWN");

    private final String description;

    public static TransactionType getByName(final String name) {
        return Arrays.stream(TransactionType.values())
                .filter(transactionType -> transactionType.name().equalsIgnoreCase(name))
                .findFirst()
                .orElse(UNKNOWN);
    }

}