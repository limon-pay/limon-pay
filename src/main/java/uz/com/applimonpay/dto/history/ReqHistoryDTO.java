package uz.com.applimonpay.dto.history;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class ReqHistoryDTO {

    @JsonProperty("from")
    private String from;

    @JsonProperty("to")
    private String to;

}
