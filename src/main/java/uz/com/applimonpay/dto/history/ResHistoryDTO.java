package uz.com.applimonpay.dto.history;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.com.applimonpay.dto.BaseAmount;
import uz.com.applimonpay.entity.Transaction;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResHistoryDTO {

    @JsonProperty("params")
    private List<ResHistoryParamsDTO> params;

    @JsonProperty("debit")
    private BaseAmount debit;

    @JsonProperty("credit")
    private BaseAmount credit;

}
