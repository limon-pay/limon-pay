package uz.com.applimonpay.dto.history;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.com.applimonpay.enums.CurrencyType;
import uz.com.applimonpay.enums.OperType;
import uz.com.applimonpay.enums.TransactionStatus;
import uz.com.applimonpay.enums.TransactionType;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResHistoryParamsDTO {

    private LocalDateTime tCreatedAt;
    private Long tAmount;
    private CurrencyType tCurrency;
    private Long oCommission;
    private OperType oType;
    private TransactionType tType;
    private TransactionStatus tStatus;

}
