package uz.com.applimonpay.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.com.applimonpay.enums.CurrencyType;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BaseAmount {

    private Long amount;

    private Short scale;

    private CurrencyType currency;

    public BaseAmount(Long amount) {
        this.amount = amount;
        this.scale = 2;
        this.currency = CurrencyType.UZS;
    }

    public BaseAmount(Long amount, CurrencyType currency) {
        this.amount = amount;
        this.scale = 2;
        this.currency = currency;
    }

}

