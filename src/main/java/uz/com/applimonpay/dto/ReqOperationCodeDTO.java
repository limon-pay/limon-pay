package uz.com.applimonpay.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ReqOperationCodeDTO {

    @JsonProperty("code")
    private String code;

}
