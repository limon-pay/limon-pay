package uz.com.applimonpay.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AttachRoleToUserDTO {

    private String phone;
    private String roleName;

}
