package uz.com.applimonpay.dto.card;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import uz.com.applimonpay.dto.user.UserDTO;
import uz.com.applimonpay.enums.CardType;
import uz.com.applimonpay.enums.CurrencyType;

import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties({"id", "pan", "expiry", "currency", "phone", "balance", "uuid", "user", "deleted", "cardToken", "createdAt", "updatedAt"})
public class CardDTO {

    @JsonProperty("id")
    private Long id;

    @JsonProperty("pan")
    private String pan;

    @JsonProperty("expiry")
    private String expiry;

    @JsonProperty("maskedPan")
    private String maskedPan;

    @JsonProperty("cardHolderName")
    private String cardHolderName;

    @JsonProperty("type")
    private CardType type;

    @JsonProperty("currency")
    private CurrencyType currency;

    @JsonProperty("phone")
    private String phone;

    @JsonProperty("balance")
    private Long balance;

    @JsonProperty("user")
    private UserDTO user;

    @JsonProperty("active")
    private boolean active;

    @JsonProperty("deleted")
    private boolean deleted;

    @JsonProperty("uuid")
    private UUID uuid;

    @JsonProperty("cardToken")
    private String cardToken;

    @JsonProperty("createdAt")
    protected LocalDateTime createdAt;

    @JsonProperty("updatedAt")
    protected LocalDateTime updatedAt;

}
