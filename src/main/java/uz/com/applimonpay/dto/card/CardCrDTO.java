package uz.com.applimonpay.dto.card;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import uz.com.applimonpay.enums.CardType;
import uz.com.applimonpay.enums.CurrencyType;

@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class CardCrDTO {

    @JsonProperty("pan")
    private String pan;

    @JsonProperty("expiry")
    private String expiry;

    @JsonProperty("cardHolderName")
    private String cardHolderName;

    @JsonProperty("type")
    private CardType type;

    @JsonProperty("currency")
    private CurrencyType currency;

    @JsonProperty("phone")
    private String phone;

    @JsonProperty("userUuid")
    private String userUuid;

}
