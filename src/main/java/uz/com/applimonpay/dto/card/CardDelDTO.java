package uz.com.applimonpay.dto.card;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.UUID;

@Getter
@Setter
@ToString
@JsonIgnoreProperties("deleted")
public class CardDelDTO {

    @JsonProperty("uuid")
    private UUID uuid;

    @JsonProperty("phone")
    private String phone;

    @JsonProperty("pan")
    private String pan;

    @JsonProperty("deleted")
    private boolean deleted = true;

}
