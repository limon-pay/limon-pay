package uz.com.applimonpay.dto.card;

import lombok.Getter;
import lombok.Setter;
import uz.com.applimonpay.enums.CardType;

@Getter
@Setter
public class UserCardDTO {

    private String token;

    private String pan;

    private String expiry;

    private String currency;

    private String owner;

    private CardType type;

}
