package uz.com.applimonpay.dto.transaction;

import lombok.*;
import uz.com.applimonpay.dto.BaseAmount;
import uz.com.applimonpay.enums.TransactionType;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class TransactionHistoryParam {

    private String sender;

    private String receiver;

    private BaseAmount amount;

    private String operationTime;

    private TransactionType transactionType;

}
