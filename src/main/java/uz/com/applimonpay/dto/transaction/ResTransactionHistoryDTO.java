package uz.com.applimonpay.dto.transaction;

import lombok.*;
import uz.com.applimonpay.dto.BaseAmount;

import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ResTransactionHistoryDTO {

    private List<TransactionHistoryParam> params;

    private BaseAmount totalDebit;

    private BaseAmount totalCredit;

}
