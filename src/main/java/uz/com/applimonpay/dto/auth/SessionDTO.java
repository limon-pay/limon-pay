package uz.com.applimonpay.dto.auth;

import lombok.*;
import uz.com.applimonpay.entity.User;

@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SessionDTO {

    private Long expireIn;

    private String accessToken;

    private String refreshToken;

    private User user;

}
