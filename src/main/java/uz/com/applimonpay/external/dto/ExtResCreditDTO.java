package uz.com.applimonpay.external.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ExtResCreditDTO {

    private String responseId;

    private String message;

}
