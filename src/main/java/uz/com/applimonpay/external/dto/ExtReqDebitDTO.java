package uz.com.applimonpay.external.dto;

import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ExtReqDebitDTO {

    private String cardToken;

    private String merchantId;

    private String terminalId;

    private Long amount;

    private String requestId;

}
