package uz.com.applimonpay.external;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import uz.com.applimonpay.entity.Card;
import uz.com.applimonpay.external.dto.ExtReqCreditDTO;
import uz.com.applimonpay.external.dto.ExtReqDebitDTO;
import uz.com.applimonpay.external.dto.ExtResCreditDTO;
import uz.com.applimonpay.external.dto.ExtResDebitDTO;
import uz.com.applimonpay.helper.Utils;
import uz.com.applimonpay.repo.CardRepo;
import uz.com.applimonpay.service.CardServ;

@Service
@RequiredArgsConstructor
public class UzcardService {
    private final CardServ cardServ;
    private final CardRepo repo;

    public ExtResDebitDTO debit(ExtReqDebitDTO dto) throws Exception {

        Card senderCard = cardServ.findByCardToken(dto.getCardToken());
        Long summa = dto.getAmount();
        Long senderBalance = senderCard.getBalance();
        ExtResDebitDTO result = new ExtResDebitDTO();

        if (!Utils.isActive(senderCard)) {
            result.setResponseId("ResId_" + System.currentTimeMillis());
            result.setMessage("failed");
        } else if (senderBalance > summa) {
            senderCard.setBalance(senderBalance - summa);
            repo.saveAndFlush(senderCard);
            result.setResponseId("ResId_" + System.currentTimeMillis());
            result.setMessage("success");
        } else if (senderBalance < summa) {
            result.setResponseId("ResId_" + System.currentTimeMillis());
            result.setMessage("failed");
        }
        return result;
    }

    public ExtResCreditDTO credit(ExtReqCreditDTO dto) throws Exception {

        Card receiverCard = cardServ.findByCardToken(dto.getCardToken());
        Long summa = dto.getAmount();
        Long receiverBalance = receiverCard.getBalance();
        ExtResCreditDTO result = new ExtResCreditDTO();

        if (!Utils.isActive(receiverCard)) {
            result.setResponseId("ResId_" + System.currentTimeMillis());
            result.setMessage("failed");
        } else {
            receiverCard.setBalance(receiverBalance + summa);
            repo.saveAndFlush(receiverCard);
            result.setResponseId("ResId_" + System.currentTimeMillis());
            result.setMessage("success");
        }
        return result;
    }
}
