package uz.com.applimonpay.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.com.applimonpay.base.BaseURI;
import uz.com.applimonpay.common.ResponseData;

@RestController
@RequiredArgsConstructor
@RequestMapping(BaseURI.API1 + BaseURI.PUBLIC)
public class PublicController {

    @GetMapping(BaseURI.CHECK)
    public ResponseEntity<ResponseData<Boolean>> test() {
        return ResponseData.success200(true);
    }

    @GetMapping(BaseURI.CHECK2)
    public ResponseEntity<ResponseData<Boolean>> test2() {
        return ResponseData.success200(true);
    }

}
