package uz.com.applimonpay.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import uz.com.applimonpay.base.BaseURI;
import uz.com.applimonpay.common.ResponseData;
import uz.com.applimonpay.dto.user.UserDTO;
import uz.com.applimonpay.service.UserServ;

@RestController
@RequiredArgsConstructor
@RequestMapping(BaseURI.API + BaseURI.V1 + BaseURI.LOGIN)
@Api(value = "LOGIN API", description = "For the LOGIN")
public class LoginContr {

    private final UserServ serv;

    /*@PreAuthorize(value = ("hasAuthority('LOGIN')"))
    @ApiOperation(value = "Get LOGIN")
    @GetMapping()
    public ResponseEntity<ResponseData<UserDTO>> login(@RequestParam(value = "phone") String phone,
                                                       @RequestParam(value = "password") String password) {
        return serv.getLogin(phone, password);
    }*/
}
