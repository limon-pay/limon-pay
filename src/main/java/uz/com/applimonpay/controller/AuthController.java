package uz.com.applimonpay.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.com.applimonpay.base.BaseURI;
import uz.com.applimonpay.dto.auth.ReqLoginDTO;
import uz.com.applimonpay.dto.auth.ReqRefreshTokenDTO;
import uz.com.applimonpay.dto.auth.SessionDTO;
import uz.com.applimonpay.service.UserServ;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequiredArgsConstructor
@RequestMapping(BaseURI.API1 + BaseURI.PUBLIC)
public class AuthController {

    private final UserServ serv;

    @PostMapping(BaseURI.LOGIN)
    public ResponseEntity<SessionDTO> login(@RequestBody ReqLoginDTO req, HttpServletRequest httpReq) {
        return serv.login(req, httpReq);
    }

    @PostMapping(BaseURI.TOKEN + BaseURI.REFRESH)
    public ResponseEntity<SessionDTO> refreshToken(@RequestBody ReqRefreshTokenDTO req, HttpServletRequest httpReq) {
        return serv.refreshToken(req, httpReq);
    }

}
