package uz.com.applimonpay.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.com.applimonpay.base.BaseURI;
import uz.com.applimonpay.common.ResponseData;
import uz.com.applimonpay.dto.card.CardCrDTO;
import uz.com.applimonpay.dto.card.CardDTO;
import uz.com.applimonpay.dto.card.CardDelDTO;
import uz.com.applimonpay.dto.card.CardUpDTO;
import uz.com.applimonpay.service.CardServ;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping(BaseURI.API + BaseURI.V1 + BaseURI.CARD)
@Api(value = "CARD APIS", description = "For the CARD category")
public class CardContr {

    private final CardServ serv;

    //    @PreAuthorize("hasAuthority('GET_ALL_CARDS')")
    @ApiOperation(value = "Get all CARDS")
    @GetMapping(BaseURI.GET + BaseURI.ALL)
    public ResponseEntity<ResponseData<List<CardDTO>>> getAll() {
        return serv.getAll();
    }

    //    @PreAuthorize("hasAuthority('GET_CARD')")
    @ApiOperation(value = "Get CARD")
    @GetMapping(BaseURI.GET)
    public ResponseEntity<ResponseData<CardDTO>> get(@RequestParam(value = "uuid") UUID uuid) {
        return serv.get(uuid);
    }

    //    @PreAuthorize("hasAuthority('ADD_CARD')")
    @ApiOperation(value = "Create CARD")
    @PostMapping(BaseURI.ADD)
    public ResponseEntity<ResponseData<CardDTO>> add(@Valid @RequestBody CardCrDTO dto) throws Exception {
        return serv.add(dto);
    }

    //    @PreAuthorize("hasAuthority('EDIT_CARD')")
    @ApiOperation(value = "Update CARD")
    @PostMapping(BaseURI.EDIT)
    public ResponseEntity<ResponseData<CardDTO>> edit(@Valid @RequestBody CardUpDTO dto) {
        return serv.edit(dto);
    }

    //    @PreAuthorize("hasAuthority('DELETE_CARD')")
    @ApiOperation(value = "Delete CARD")
    @PostMapping(BaseURI.DELETE)
    public ResponseEntity<ResponseData<CardDTO>> delete(@Valid @RequestBody CardDelDTO dto) {
        return serv.delete(dto);
    }
}
