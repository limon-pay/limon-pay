package uz.com.applimonpay.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.com.applimonpay.base.BaseController;
import uz.com.applimonpay.base.BaseURI;
import uz.com.applimonpay.common.ResponseData;
import uz.com.applimonpay.dto.ReqOperationCodeDTO;
import uz.com.applimonpay.dto.ResOperationCodeDTO;
import uz.com.applimonpay.service.OperCodeServ;

@RestController
@RequiredArgsConstructor
@RequestMapping(BaseURI.API1 + BaseURI.TEST)
public class TestController extends BaseController {

    private final OperCodeServ service;

    @PostMapping(BaseURI.GET + BaseURI.OPERATION_CODE)
    public ResponseEntity<ResponseData<ResOperationCodeDTO>> getOperationCode(@RequestBody ReqOperationCodeDTO dto) {
        return ResponseData.success200(service.getOperationCode(dto));
    }

}
