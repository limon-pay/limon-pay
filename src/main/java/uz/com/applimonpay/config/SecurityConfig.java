package uz.com.applimonpay.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import uz.com.applimonpay.base.BaseURI;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private final UserDetailsService userDetailsService;

    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth
                .userDetailsService(userDetailsService)
        /*.passwordEncoder(this.passwordEncoder())*/;
                /*.inMemoryAuthentication()
                .withUser("admin").password(passwordEncoder().encode("1111"))
                .roles("ADMIN")
                .authorities(
                        "GET_ALL_USERS",
                        "GET_USER",
                        "ADD_USER",
                        "EDIT_USER",
                        "DELETE_USER",

                        "GET_ALL_CARDS",
                        "GET_CARD",
                        "ADD_CARD",
                        "EDIT_CARD",
                        "DELETE_CARD"
                )
                .and()
                .withUser("user").password(passwordEncoder().encode("2222"))
                .roles("USER")
                .authorities(
                        "LOGIN",
                        "GET_ALL_USERS",
                        "GET_USER",
                        "ADD_USER",
                        "EDIT_USER",
                        "DELETE_USER",
                        "USER_ME",
                        "CHANGE_PASSWORD",

                        "GET_ALL_CARDS",
                        "GET_CARD",
                        "ADD_CARD",
                        "EDIT_CARD",
                        "DELETE_CARD",

                        "HOLD",
                        "CONFIRM",
                        "HISTORY"
                )
                .and()
                .withUser("guest").password(passwordEncoder().encode("3333"))
                .roles("GUEST")
                .authorities("GET_ALL_USERS");*/
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        http.authorizeRequests()
                .antMatchers(BaseURI.API1 + BaseURI.PUBLIC + "/**").permitAll()
                .antMatchers(BaseURI.API1 + BaseURI.TEST + "/**").permitAll()
                .antMatchers(BaseURI.API1 + BaseURI.CARD + "/**").permitAll()
                .anyRequest().authenticated();
        http.addFilter(new CustomAuthenticationFilter(authenticationManagerBean()));
        http.addFilterBefore(new CustomAuthorizationFilter(), UsernamePasswordAuthenticationFilter.class);
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http.httpBasic();

//                .antMatchers("/login", "/logout", "/swagger-ui.html").permitAll()
//                .antMatchers(BaseURI.API1 + BaseURI.USER + "/**").hasRole("USER")
//                .antMatchers(BaseURI.API1 + BaseURI.CARD + "/**").hasRole("ADMIN")
                /*.antMatchers(BaseURI.API1 + BaseURI.USER + "/**").hasAnyRole("USER", "ADMIN")
                .antMatchers(BaseURI.API1 + BaseURI.CARD + "/**").hasAnyRole("USER", "ADMIN")
                .antMatchers(BaseURI.API1 + BaseURI.TRANSACTION + "/**").hasAnyRole("USER", "ADMIN")
                .antMatchers(BaseURI.API1 + BaseURI.USER + BaseURI.GET + BaseURI.ALL).hasRole("GUEST")*/
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
}
