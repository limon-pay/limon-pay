package uz.com.applimonpay.base;

public interface BaseURI {

    String API = "/api";
    String V1 = "/v1";

    String API1 = API + V1;

    String PUBLIC = "/public";
    String TRANSACTION = "/transaction";
    String TEST = "/test";
    String OPERATION_CODE = "/operationCode";

    String USER = "/user";
    String ROLE = "/role";
    String CARD = "/card";

    String LOGIN = "/login";
    String TOKEN = "/token";
    String REFRESH = "/refresh";
    String ME = "/me";
    String CHANGEPASSWORD = "/changepassword";
    String ATTACH = "/attach";
    String TO = "/to";
    String GET = "/get";
    String ALL = "/all";
    String ADD = "/add";
    String EDIT = "/edit";
    String DELETE = "/delete";

    String HOLD = "/hold";
    String CONFIRM = "/confirm";
    String HISTORY = "/history";
    String CHECK = "/check";
    String CHECK2 = "/check2";


}
