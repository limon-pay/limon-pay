package uz.com.applimonpay.repo;

import org.springframework.stereotype.Repository;
import uz.com.applimonpay.base.BaseRepo;
import uz.com.applimonpay.entity.Epos;
import uz.com.applimonpay.enums.TransactionType;

import java.util.Optional;

@Repository
public interface EposRepo extends BaseRepo<Epos> {

    Optional<Epos> findByOperationCodeAndTransactionType(String code, TransactionType transactionType);

}
