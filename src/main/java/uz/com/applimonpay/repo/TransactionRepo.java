package uz.com.applimonpay.repo;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.com.applimonpay.base.BaseRepo;
import uz.com.applimonpay.entity.Transaction;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface TransactionRepo extends BaseRepo<Transaction> {

    @Query("SELECT t FROM Transaction t " +
            " INNER JOIN t.operation o " +
            " WHERE (o.senderPan = :pan OR o.receiverPan = :pan) " +
            "   AND o.endTime BETWEEN :startDate AND :endDate " +
            "   AND o.status = 'SUCCESS' ")
    List<Transaction> getAllHistory(String pan, LocalDateTime startDate, LocalDateTime endDate);
}
