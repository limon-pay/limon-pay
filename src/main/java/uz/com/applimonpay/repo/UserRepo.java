package uz.com.applimonpay.repo;

import org.springframework.stereotype.Repository;
import uz.com.applimonpay.base.BaseRepo;
import uz.com.applimonpay.entity.User;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface UserRepo extends BaseRepo<User> {
    List<User> findAllByDeletedIsFalseAndActiveIsTrue();

    Optional<User> findByUuid(UUID uuid);

    Optional<User> findByPhoneAndPassword(String phone, String password);

    Optional<User> findByPhone(String phone);
}
