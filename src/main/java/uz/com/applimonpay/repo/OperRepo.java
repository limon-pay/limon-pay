package uz.com.applimonpay.repo;

import org.springframework.stereotype.Repository;
import uz.com.applimonpay.base.BaseRepo;
import uz.com.applimonpay.entity.Operation;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface OperRepo extends BaseRepo<Operation> {
    Optional<Operation> findByUuid(UUID operationUuid);

}
