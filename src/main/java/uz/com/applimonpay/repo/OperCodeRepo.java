package uz.com.applimonpay.repo;

import org.springframework.stereotype.Repository;
import uz.com.applimonpay.base.BaseRepo;
import uz.com.applimonpay.entity.OperationCode;

import java.util.Optional;

@Repository
public interface OperCodeRepo extends BaseRepo<OperationCode> {
    Optional<OperationCode> findByCode(String code);
}
