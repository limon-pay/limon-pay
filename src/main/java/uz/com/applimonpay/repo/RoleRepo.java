package uz.com.applimonpay.repo;

import uz.com.applimonpay.base.BaseRepo;
import uz.com.applimonpay.entity.Role;

import java.util.Optional;

public interface RoleRepo extends BaseRepo<Role> {

    Role findByName(String name);

}
