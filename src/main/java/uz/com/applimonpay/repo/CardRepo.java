package uz.com.applimonpay.repo;

import org.springframework.stereotype.Repository;
import uz.com.applimonpay.base.BaseRepo;
import uz.com.applimonpay.entity.Card;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface CardRepo extends BaseRepo<Card> {

    Optional<Card> findByPhone(String phone);

    Optional<Card> findByPhoneAndUuid(String phone, UUID uuid);

    List<Card> findAllByDeletedIsFalseAndActiveIsTrue();

    Optional<Card> findByUuid(UUID uuid);

    Optional<Card> findByPanAndUuid(String pan, UUID uuid);

    List<Card> findAllByUserId(Long id);

    Optional<Card> findByPan(String pan);

    Optional<Card> findByCardToken(String cardToken);
}
