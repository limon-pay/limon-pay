package uz.com.applimonpay.exception;

public class PanShouldNotBeNullException extends Exception {
    public PanShouldNotBeNullException(String message) {
        super(message);
    }

}
