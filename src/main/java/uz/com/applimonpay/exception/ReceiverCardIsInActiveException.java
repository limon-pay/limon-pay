package uz.com.applimonpay.exception;

public class ReceiverCardIsInActiveException extends Exception{

    public ReceiverCardIsInActiveException(String message) {
        super(message);
    }
}
