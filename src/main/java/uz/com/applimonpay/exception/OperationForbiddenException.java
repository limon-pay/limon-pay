package uz.com.applimonpay.exception;

public class OperationForbiddenException extends Exception {

    public OperationForbiddenException(String message) {
        super(message);
    }

}
