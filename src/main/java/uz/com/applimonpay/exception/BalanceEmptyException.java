package uz.com.applimonpay.exception;

public class BalanceEmptyException extends Exception {
    public BalanceEmptyException(String message) {
        super(message);
    }
}
