package uz.com.applimonpay.exception;

public class CardTokenNotFoundException extends Exception{

    public CardTokenNotFoundException(String message) {
        super(message);
    }
}
