package uz.com.applimonpay.exception;

public class OperationCodeNotFoundException extends Exception{

    public OperationCodeNotFoundException(String message) {
        super(message);
    }
}
