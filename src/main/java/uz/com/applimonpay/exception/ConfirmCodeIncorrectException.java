package uz.com.applimonpay.exception;

public class ConfirmCodeIncorrectException extends Exception{

    public ConfirmCodeIncorrectException(String message) {
        super(message);
    }
}
