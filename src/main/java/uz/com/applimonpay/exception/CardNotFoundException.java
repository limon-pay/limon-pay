package uz.com.applimonpay.exception;

public class CardNotFoundException extends Exception{

    public CardNotFoundException(String message) {
        super(message);
    }
}
