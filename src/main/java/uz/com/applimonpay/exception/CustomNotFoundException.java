package uz.com.applimonpay.exception;

public class CustomNotFoundException extends Exception {
    public CustomNotFoundException(String message) {
        super(message);
    }
}
