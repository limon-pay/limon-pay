package uz.com.applimonpay.exception;

public class SenderCardIsInActiveException extends Exception{

    public SenderCardIsInActiveException(String message) {
        super(message);
    }
}
