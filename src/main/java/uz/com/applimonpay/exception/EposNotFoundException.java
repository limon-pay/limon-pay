package uz.com.applimonpay.exception;

public class EposNotFoundException extends Exception{

    public EposNotFoundException(String message) {
        super(message);
    }
}
