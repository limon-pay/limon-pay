package uz.com.applimonpay.exception;

public class OperationTypeNotFoundException extends Exception {

    public OperationTypeNotFoundException(String message) {
        super(message);
    }

}
