package uz.com.applimonpay.exception;

public class OperationNotFoundException extends Exception{

    public OperationNotFoundException(String message) {
        super(message);
    }
}
