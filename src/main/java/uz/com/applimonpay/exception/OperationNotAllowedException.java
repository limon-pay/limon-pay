package uz.com.applimonpay.exception;

public class OperationNotAllowedException extends Exception {

    public OperationNotAllowedException(String message) {
        super(message);
    }
}
