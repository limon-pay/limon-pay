package uz.com.applimonpay.exception;

public class AmountNotFoundException extends Exception{

    public AmountNotFoundException(String message) {
        super(message);
    }
}
