package uz.com.applimonpay.helper;

import org.springframework.util.StringUtils;
import uz.com.applimonpay.entity.Card;
import uz.com.applimonpay.enums.CardType;
import uz.com.applimonpay.enums.FieldGroup;
import uz.com.applimonpay.enums.OperType;
import uz.com.applimonpay.exception.OperationTypeNotFoundException;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

public class Utils {

    public static boolean isUzcard(CardType type) {
        return CardType.UZCARD.equals(type);
    }

    public static boolean isUzcard(String pan) {
        if (isPresent(pan) && pan.length() == 16) {
            return pan.startsWith("8600");
        }
        return false;
    }

    public static boolean isHumo(CardType type) {
        return CardType.HUMO.equals(type);
    }

    public static boolean isHumo(String pan) {
        if (isPresent(pan) && pan.length() == 16) {
            return pan.startsWith("9860");
        }
        return false;
    }

    public static boolean isVisa(CardType type) {
        return CardType.VISA.equals(type);
    }

    public static boolean isVisa(String pan) {
        if (isPresent(pan) && pan.length() == 16) {
            return pan.startsWith("4000") || pan.startsWith("4200");
        }
        return false;
    }

    public static boolean checkConfirmCode(String operConfirmCode, String dtoConfirmCode) {
        return operConfirmCode.equals(dtoConfirmCode);
    }

    public static boolean isActive(Card card) {
        return card.isActive();
    }

    public static boolean isOwner(String pan) {

        if (isPresent(pan) && pan.length() == 16) {
            if (pan.startsWith("860013") || pan.startsWith("986003") || pan.startsWith("400033")) {
                return true;
            }
        }
        return false;
    }

    public static OperType operationType(FieldGroup sender, FieldGroup receiver) throws OperationTypeNotFoundException {
        if (FieldGroup.CARD.equals(sender) && FieldGroup.CARD.equals(receiver)) {
            return OperType.P2P;
        } else if (FieldGroup.CARD.equals(sender) && FieldGroup.SERVICE.equals(receiver)) {
            return OperType.P2S;
        }
        throw new OperationTypeNotFoundException("Operation type not found !!!");
    }


    public static Long calculateCommission(Long amount, Integer commissionRate) {
        if (Utils.isPresent(amount) && Utils.isPresent(commissionRate)) {
            return amount * commissionRate / 100L;
        }
        return 0L;
    }

    public static Integer randomNumber() {
        return (int) (Math.random() * 999999);
    }

    public static final DateTimeFormatter formatLocalDateTime = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");
    public static final DateTimeFormatter formatLocal = DateTimeFormatter.ofPattern("dd.MM.yyyy");

    public static LocalDateTime toLocalDateTime(String stringTime) {
        return LocalDateTime.parse(stringTime, formatLocalDateTime);
    }

    public static LocalDate toLocalDate(String stringDate) {
        return LocalDate.parse(stringDate, formatLocal);
    }

    public static boolean isEmpty(Object obj) {
        return obj == null;
    }

    public static boolean isEmpty(String str) {
        return !StringUtils.hasText(str);
    }

    public static boolean isEmpty(List<?> list) {
        return list == null || list.isEmpty();
    }

    public static boolean isEmpty(Map<?, ?> map) {
        return map == null || map.isEmpty();
    }

    public static boolean isPresent(Object obj) {
        return obj != null;
    }

    public static boolean isPresent(String str) {
        return StringUtils.hasText(str);
    }

    public static boolean isPresent(List<?> list) {
        return list != null && !list.isEmpty();
    }

    public static boolean isPresent(Map<?, ?> map) {
        return map != null && !map.isEmpty();
    }
}
