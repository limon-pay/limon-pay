package uz.com.applimonpay.service;

import uz.com.applimonpay.entity.Epos;
import uz.com.applimonpay.enums.TransactionType;

public interface EposServ {
    Epos findByOperationCodeAndTransactionType(String code, TransactionType transactionType) throws Exception;
}
