package uz.com.applimonpay.service;

import org.springframework.http.ResponseEntity;
import uz.com.applimonpay.common.ResponseData;
import uz.com.applimonpay.dto.card.*;
import uz.com.applimonpay.dto.transaction.ReqTransactionParam;
import uz.com.applimonpay.entity.Card;
import uz.com.applimonpay.exception.CardNotFoundException;
import uz.com.applimonpay.exception.PanShouldNotBeNullException;

import java.util.List;
import java.util.UUID;

public interface CardServ {

    Card findByUuid(UUID uuid) throws CardNotFoundException;

    Card findByPan(String pan) throws CardNotFoundException, PanShouldNotBeNullException;

    UserCardDTO getCard(ReqTransactionParam param) throws CardNotFoundException;

    ResponseEntity<ResponseData<List<CardDTO>>> getAll();

    ResponseEntity<ResponseData<CardDTO>> get(UUID uuid);

    UserCardDTO getUserCards(UUID uuid) throws CardNotFoundException;

    ResponseEntity<ResponseData<CardDTO>> add(CardCrDTO dto) throws Exception;

    ResponseEntity<ResponseData<CardDTO>> edit(CardUpDTO dto);

    ResponseEntity<ResponseData<CardDTO>> delete(CardDelDTO dto);

    Card findByCardToken(String cardToken) throws CardNotFoundException;


}
