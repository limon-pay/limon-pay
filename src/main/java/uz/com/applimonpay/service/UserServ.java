package uz.com.applimonpay.service;

import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import uz.com.applimonpay.common.ResponseData;
import uz.com.applimonpay.dto.auth.ReqLoginDTO;
import uz.com.applimonpay.dto.auth.ReqRefreshTokenDTO;
import uz.com.applimonpay.dto.auth.SessionDTO;
import uz.com.applimonpay.dto.user.*;
import uz.com.applimonpay.entity.Role;
import uz.com.applimonpay.entity.User;
import uz.com.applimonpay.exception.UserNotFoundException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.UUID;

public interface UserServ {
    ResponseEntity<ResponseData<List<UserDTO>>> getAll();

    ResponseEntity<ResponseData<UserDTO>> get(UUID uuid);

    ResponseEntity<ResponseData<UserDTO>> add(UserCrDTO dto);

    ResponseEntity<ResponseData<UserDTO>> edit(UserUpDTO dto);

    ResponseEntity<ResponseData<UserDTO>> me(UserMeDTO dto);

    ResponseEntity<ResponseData<UserDTO>> delete(UserDelDTO dto);

    ResponseEntity<ResponseData<UserDTO>> changePassword(UserChPDTO dto);

    User findByUuid(UUID uuid) throws UserNotFoundException;

    ResponseEntity<SessionDTO> login(ReqLoginDTO req, HttpServletRequest httpReq);

    ResponseEntity<SessionDTO> refreshToken(ReqRefreshTokenDTO req, HttpServletRequest httpReq);

    User findByPhone(String phone) throws UsernameNotFoundException;

    Role saveRole(Role role);

    void attachRoleToUser(String phone, String roleName);
}
