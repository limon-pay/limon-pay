package uz.com.applimonpay.service;

import uz.com.applimonpay.base.BaseService;
import uz.com.applimonpay.entity.Operation;
import uz.com.applimonpay.exception.OperationNotFoundException;

import java.util.UUID;

public interface OperServ extends BaseService {

    Operation save(Operation operation);

    Operation findByUuid(UUID uuid) throws OperationNotFoundException;

}
