package uz.com.applimonpay.service;

import uz.com.applimonpay.dto.ReqOperationCodeDTO;
import uz.com.applimonpay.dto.ResOperationCodeDTO;
import uz.com.applimonpay.dto.card.UserCardDTO;
import uz.com.applimonpay.exception.OperationCodeNotFoundException;
import uz.com.applimonpay.exception.PanShouldNotBeNullException;

public interface OperCodeServ {

    String findOperation(UserCardDTO sender, UserCardDTO receiver) throws OperationCodeNotFoundException, PanShouldNotBeNullException;

    ResOperationCodeDTO findByCode(String code);

    ResOperationCodeDTO getOperationCode(ReqOperationCodeDTO dto);
}
