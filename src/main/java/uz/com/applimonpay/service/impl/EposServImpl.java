package uz.com.applimonpay.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import uz.com.applimonpay.entity.Epos;
import uz.com.applimonpay.enums.TransactionType;
import uz.com.applimonpay.exception.EposNotFoundException;
import uz.com.applimonpay.helper.Utils;
import uz.com.applimonpay.repo.EposRepo;
import uz.com.applimonpay.service.EposServ;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class EposServImpl implements EposServ {

    private final EposRepo repo;

    public Epos findByOperationCodeAndTransactionType(String code, TransactionType transactionType) throws EposNotFoundException {
        Optional<Epos> optional = repo.findByOperationCodeAndTransactionType(code, transactionType);
        if (Utils.isEmpty(optional)) {
            throw new EposNotFoundException("Epos is not found !!!");
        }
        return optional.get();
    }


}
