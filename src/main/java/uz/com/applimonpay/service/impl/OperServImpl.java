package uz.com.applimonpay.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import uz.com.applimonpay.entity.Operation;
import uz.com.applimonpay.exception.OperationNotFoundException;
import uz.com.applimonpay.repo.OperRepo;
import uz.com.applimonpay.service.OperServ;

import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class OperServImpl implements OperServ {

    private final OperRepo repo;

    @Override
    public Operation save(Operation operation) {
        return repo.saveAndFlush(operation);
    }

    @Override
    public Operation findByUuid(UUID uuid) throws OperationNotFoundException {
        Optional<Operation> operation = repo.findByUuid(uuid);
        if (operation.isEmpty()) {
            throw new OperationNotFoundException("Card is not found");
        }
        return operation.get();
    }

}
