package uz.com.applimonpay.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import uz.com.applimonpay.dto.BaseAmount;
import uz.com.applimonpay.dto.ResOperationCodeDTO;
import uz.com.applimonpay.dto.card.UserCardDTO;
import uz.com.applimonpay.dto.transaction.*;
import uz.com.applimonpay.entity.*;
import uz.com.applimonpay.enums.*;
import uz.com.applimonpay.exception.*;
import uz.com.applimonpay.external.HumoService;
import uz.com.applimonpay.external.UzcardService;
import uz.com.applimonpay.external.VisaService;
import uz.com.applimonpay.external.dto.ExtReqCreditDTO;
import uz.com.applimonpay.external.dto.ExtReqDebitDTO;
import uz.com.applimonpay.external.dto.ExtResCreditDTO;
import uz.com.applimonpay.external.dto.ExtResDebitDTO;
import uz.com.applimonpay.helper.UserSession;
import uz.com.applimonpay.helper.Utils;
import uz.com.applimonpay.repo.TransactionRepo;
import uz.com.applimonpay.service.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TransactionServImpl implements TransactionServ {

    private final TransactionRepo repo;
    private final CardServ cardServ;
    private final OperServ operServ;
    private final OperCodeServ operCodeServ;
    private final EposServ eposServ;
    private final UzcardService uzcardService;
    private final HumoService humoService;
    private final VisaService visaService;
    private final UserSession session;

    @Override
    public ResTransactionHoldDTO hold(ReqTransactionHoldDTO dto) throws Exception {

        ReqTransactionParam senderParam = dto.getSender();
        ReqTransactionParam receiverParam = dto.getReceiver();
        BaseAmount amountParam = dto.getAmount();

        if (Utils.isEmpty(senderParam) || Utils.isEmpty(receiverParam) || Utils.isEmpty(amountParam)) {
            throw new OperationForbiddenException("Required params are null !!!");
        }

        FieldGroup senderGroup = dto.getSender().getGroup();
        FieldGroup receiverGroup = dto.getReceiver().getGroup();

        User user = session.getUser();
        System.out.println("User -> " + user);

        try {

            OperType operType = Utils.operationType(senderGroup, receiverGroup);

            UserCardDTO senderCard = cardServ.getCard(senderParam);
            UserCardDTO receiverCard = cardServ.getCard(receiverParam);

            final String code = String.valueOf(operCodeServ.findOperation(senderCard, receiverCard));

            Epos debitEpos = eposServ.findByOperationCodeAndTransactionType(code, TransactionType.DEBIT);
            Epos creditEpos = eposServ.findByOperationCodeAndTransactionType(code, TransactionType.CREDIT);

            ResOperationCodeDTO operationCode = operCodeServ.findByCode(code);

            Long amount = amountParam.getAmount();
            Long commission = Utils.calculateCommission(amount, operationCode.getCommissionRate());

            Operation operation = new Operation();
            operation.setStatus(OperStatus.HOLD);
            operation.setAmount(amount);
            operation.setCurrency(amountParam.getCurrency());
            operation.setCommission(commission);
            operation.setUserId(user.getId());

            operation.setSenderPan(senderCard.getPan());
            operation.setSenderToken(senderCard.getToken());
            operation.setSenderMerchant(debitEpos.getMerchantId());
            operation.setSenderTerminal(debitEpos.getTerminalId());

            operation.setReceiverPan(receiverCard.getPan());
            operation.setReceiverToken(receiverCard.getToken());
            operation.setReceiverMerchant(creditEpos.getMerchantId());
            operation.setReceiverTerminal(creditEpos.getTerminalId());

            operation.setOperationCode(code);
            operation.setType(operType);
            operation.setStartTime(LocalDateTime.now());

            if (amount >= 1_000_000_00) {
                operation.setNeedConfirm(Boolean.TRUE);
                Integer confirmCode = Utils.randomNumber();
                operation.setConfirmCode(confirmCode.toString());
            }

            operServ.save(operation);

            return new ResTransactionHoldDTO(operation.getUuid(), operation.getNeedConfirm());

        } catch (OperationTypeNotFoundException | OperationCodeNotFoundException e) {
            throw new OperationForbiddenException(e.getMessage());
        }

    }

    @Override
    public ResTransactionConfirmDTO confirm(ReqTransactionConfirmDTO dto) throws Exception {
        try {
            Operation operation = operServ.findByUuid(dto.getOperationUuid());

            if (operation.getNeedConfirm()) {
                if (Utils.isPresent(dto.getConfirmCode())) {
                    if (!Utils.checkConfirmCode(operation.getConfirmCode(), dto.getConfirmCode())) {
                        throw new ConfirmCodeIncorrectException("Confirmation code is incorrect !!!");

                    }
                }
            }

            Transaction debitTr = new Transaction();
            debitTr.setOperationId(operation.getId());
            debitTr.setType(TransactionType.DEBIT);
            debitTr.setStatus(TransactionStatus.PENDING);
            debitTr.setRequestId("ReqId_" + System.currentTimeMillis());
            debitTr.setAmount(operation.getAmount() + operation.getCommission());
            debitTr.setCurrency(operation.getCurrency());
            repo.saveAndFlush(debitTr);

            ExtReqDebitDTO debitRequest = new ExtReqDebitDTO();
            debitRequest.setCardToken(operation.getSenderToken());
            debitRequest.setMerchantId(operation.getSenderMerchant());
            debitRequest.setTerminalId(operation.getSenderTerminal());
            debitRequest.setAmount(debitTr.getAmount());
            debitRequest.setRequestId(debitRequest.getRequestId());

            ExtResDebitDTO debitDTO;
            if (Utils.isUzcard(operation.getSenderPan())) {
                debitDTO = uzcardService.debit(debitRequest);
            } else if (Utils.isHumo(operation.getSenderPan())) {
                debitDTO = humoService.debit(debitRequest);
            } else if (Utils.isVisa(operation.getSenderPan())) {
                debitDTO = visaService.debit(debitRequest);
            } else {
                throw new OperationForbiddenException("Card type is invalid!!!");
            }

            if ("failed".equals(debitDTO.getMessage())) {

                debitTr.setResponseId(debitDTO.getResponseId());
                debitTr.setStatus(TransactionStatus.ERROR);
                debitTr.setErrorMessage("ERROR");
                repo.save(debitTr);

                operation.setStatus(OperStatus.FAILED);
                operation.setEndTime(LocalDateTime.now());
                operServ.save(operation);

                throw new TransactionFailedException("Transaction was completed with error !!!");

            } else if ("success".equals(debitDTO.getMessage())) {

                debitTr.setStatus(TransactionStatus.SUCCESS);
                debitTr.setResponseId(debitDTO.getResponseId());

                Transaction creditTr = new Transaction();
                creditTr.setOperationId(operation.getId());
                creditTr.setType(TransactionType.CREDIT);
                creditTr.setStatus(TransactionStatus.PENDING);
                creditTr.setRequestId("ReqId_" + System.currentTimeMillis());
                creditTr.setAmount(operation.getAmount());
                creditTr.setCurrency(operation.getCurrency());
                repo.saveAndFlush(creditTr);

                ExtReqCreditDTO creditRequest = new ExtReqCreditDTO();
                creditRequest.setCardToken(operation.getReceiverToken());
                creditRequest.setMerchantId(operation.getReceiverMerchant());
                creditRequest.setTerminalId(operation.getReceiverTerminal());
                creditRequest.setAmount(creditTr.getAmount());
                creditRequest.setRequestId(creditTr.getRequestId());

                ExtResCreditDTO creditDTO;
                if (Utils.isUzcard(operation.getReceiverPan())) {
                    creditDTO = uzcardService.credit(creditRequest);
                } else if (Utils.isHumo(operation.getReceiverPan())) {
                    creditDTO = humoService.credit(creditRequest);
                } else if (Utils.isVisa(operation.getReceiverPan())) {
                    creditDTO = visaService.credit(creditRequest);
                } else {
                    throw new OperationForbiddenException("Card type is invalid!!!");
                }

                if ("failed".equals(creditDTO.getMessage())) {

                    debitTr.setStatus(TransactionStatus.ERROR);
                    debitTr.setErrorMessage("ERROR");
                    debitTr.setResponseId(debitDTO.getResponseId());
                    creditTr.setStatus(TransactionStatus.ERROR);
                    creditTr.setErrorMessage("ERROR");
                    creditTr.setResponseId(creditDTO.getResponseId());
                    repo.saveAndFlush(debitTr);
                    repo.saveAndFlush(creditTr);

                    operation.setStatus(OperStatus.FAILED);
                    operation.setEndTime(LocalDateTime.now());
                    operServ.save(operation);

                    throw new TransactionFailedException("Transaction was completed with error !!!");


                } else if ("success".equals(creditDTO.getMessage())) {

                    debitTr.setStatus(TransactionStatus.SUCCESS);
                    debitTr.setResponseId(debitDTO.getResponseId());
                    creditTr.setStatus(TransactionStatus.SUCCESS);
                    creditTr.setResponseId(creditDTO.getResponseId());
                    repo.saveAndFlush(debitTr);
                    repo.saveAndFlush(creditTr);

                    operation.setStatus(OperStatus.SUCCESS);
                    operation.setEndTime(LocalDateTime.now());
                    operServ.save(operation);
                }
            }

        } catch (OperationForbiddenException e) {
            throw new OperationForbiddenException(e.getMessage());
        }

        return new ResTransactionConfirmDTO("Transaction was completed successfully !!!");
    }

    @Override
    public ResTransactionHistoryDTO history(ReqTransactionHistoryDTO dto) {

        try {

            Card card = cardServ.findByUuid(dto.getCardUid());

            LocalDateTime startDate = Utils.toLocalDateTime(dto.getStartTime());
            LocalDateTime endDate = Utils.toLocalDateTime(dto.getEndTime());

            List<TransactionHistoryParam> params = new ArrayList<>();

            long totalDebit = 0L;
            long totalCredit = 0L;

            List<Transaction> transactionList = repo.getAllHistory(card.getPan(), startDate, endDate);
            for (Transaction transaction : transactionList) {
                Operation operation = transaction.getOperation();

                TransactionHistoryParam param = new TransactionHistoryParam();
                param.setSender(operation.getSenderPan());
                param.setReceiver(operation.getReceiverPan());
                param.setAmount(new BaseAmount(operation.getAmount()));
                param.setOperationTime(operation.getEndTime().format(Utils.formatLocalDateTime));
                param.setTransactionType(transaction.getType());

                if (TransactionType.DEBIT.equals(transaction.getType())) {
                    totalDebit = totalDebit + operation.getAmount();
                } else if (TransactionType.CREDIT.equals(transaction.getType())) {
                    totalCredit = totalCredit + operation.getAmount();
                }
                params.add(param);
            }

            ResTransactionHistoryDTO result = new ResTransactionHistoryDTO();
            result.setParams(params);
            result.setTotalDebit(new BaseAmount(totalDebit));
            result.setTotalCredit(new BaseAmount(totalCredit));
            return result;

        } catch (CardNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

}
