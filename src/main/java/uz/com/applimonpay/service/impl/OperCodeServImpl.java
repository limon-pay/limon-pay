package uz.com.applimonpay.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import uz.com.applimonpay.dto.ReqOperationCodeDTO;
import uz.com.applimonpay.dto.ResOperationCodeDTO;
import uz.com.applimonpay.dto.card.UserCardDTO;
import uz.com.applimonpay.entity.OperationCode;
import uz.com.applimonpay.enums.CardType;
import uz.com.applimonpay.exception.OperationCodeNotFoundException;
import uz.com.applimonpay.helper.Utils;
import uz.com.applimonpay.repo.OperCodeRepo;
import uz.com.applimonpay.service.OperCodeServ;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class OperCodeServImpl implements OperCodeServ {

    private final OperCodeRepo repo;


    public String findOperation(UserCardDTO sender, UserCardDTO receiver) throws OperationCodeNotFoundException {

        CardType senderType = sender.getType();
        CardType receiverType = receiver.getType();
        String senderPan = sender.getPan();
        String receiverPan = receiver.getPan();

        if (Utils.isUzcard(senderType) && Utils.isUzcard(receiverType)) {

            if (Utils.isOwner(senderPan)) {
                return Utils.isOwner(receiverPan) ? "UNB2UNB" : "UNB2UNU";
            } else {
                return Utils.isOwner(receiverPan) ? "UNU2UNB" : "UNU2UNU";
            }

        } else if (Utils.isUzcard(senderType) && Utils.isHumo(receiverType)) {

            if (Utils.isOwner(senderPan)) {
                return Utils.isOwner(receiverPan) ? "UNB2HNB" : "UNB2HNU";
            } else {
                return Utils.isOwner(receiverPan) ? "UNU2HNB" : "UNU2HNU";
            }

        } else if (Utils.isUzcard(senderType) && Utils.isVisa(receiverType)) {

            if (Utils.isOwner(senderPan)) {
                return Utils.isOwner(receiverPan) ? "UNB2VNB" : "UNB2VNU";
            } else {
                return Utils.isOwner(receiverPan) ? "UNU2VNB" : "UNU2VNU";
            }

        } else if (Utils.isHumo(senderType) && Utils.isHumo(receiverType)) {

            if (Utils.isOwner(senderPan)) {
                return Utils.isOwner(receiverPan) ? "HNB2HNB" : "HNB2HNU";
            } else {
                return Utils.isOwner(receiverPan) ? "HNU2HNB" : "HNU2HNU";
            }

        } else if (Utils.isHumo(senderType) && Utils.isUzcard(receiverType)) {

            if (Utils.isOwner(senderPan)) {
                return Utils.isOwner(receiverPan) ? "HNB2UNB" : "HNB2UNU";
            } else {
                return Utils.isOwner(receiverPan) ? "HNU2UNB" : "HNU2UNU";
            }

        } else if (Utils.isHumo(senderType) && Utils.isVisa(receiverType)) {

            if (Utils.isOwner(senderPan)) {
                return Utils.isOwner(receiverPan) ? "HNB2VNB" : "HNB2VNU";
            } else {
                return Utils.isOwner(receiverPan) ? "HNU2VNB" : "HNU2VNU";
            }

        } else if (Utils.isVisa(senderType) && Utils.isVisa(receiverType)) {

            if (Utils.isOwner(senderPan)) {
                return Utils.isOwner(receiverPan) ? "VNB2VNB" : "VNB2VNU";
            } else {
                return Utils.isOwner(receiverPan) ? "VNU2VNB" : "VNU2VNU";
            }

        } else if (Utils.isVisa(senderType) && Utils.isUzcard(receiverType)) {

            if (Utils.isOwner(senderPan)) {
                return Utils.isOwner(receiverPan) ? "VNB2UNB" : "VNB2UNU";
            } else {
                return Utils.isOwner(receiverPan) ? "VNU2UNB" : "VNU2UNU";
            }

        } else if (Utils.isVisa(senderType) && Utils.isHumo(receiverType)) {

            if (Utils.isOwner(senderPan)) {
                return Utils.isOwner(receiverPan) ? "VNB2HNB" : "VNB2HNU";
            } else {
                return Utils.isOwner(receiverPan) ? "VNU2HNB" : "VNU2HNU";
            }

        }

        throw new OperationCodeNotFoundException("Operation code not found!!!");

    }

    @Cacheable(cacheNames = "OperCodeServ.findByCode", key = "#code", unless = "#result == null")
    @Override
    public ResOperationCodeDTO findByCode(String code) {

        try {
            Thread.sleep(5 * 1000); // 5 seconds
        } catch (InterruptedException e) {
            // ignored
        }

        Optional<OperationCode> optional = repo.findByCode(code);
        if (optional.isPresent()) {
            OperationCode operationCode = optional.get();

            ResOperationCodeDTO result = new ResOperationCodeDTO();
            result.setId(operationCode.getId());
            result.setCode(operationCode.getCode());
            result.setName(operationCode.getName());
            result.setCommissionRate(operationCode.getCommissionRate());

            return result;
        }

        return null;
    }

    @Cacheable(cacheNames = "OperationCodeService.getOperationCode", key = "#dto.code", unless = "#result == null")
    @Override
    public ResOperationCodeDTO getOperationCode(ReqOperationCodeDTO dto) {

        try {
            Thread.sleep(5 * 1000); // 5 seconds
        } catch (InterruptedException e) {
            // ignored
        }

        Optional<OperationCode> operationCodeOptional = repo.findByCode(dto.getCode());
        if (operationCodeOptional.isPresent()) {

            OperationCode operationCode = operationCodeOptional.get();

            ResOperationCodeDTO result = new ResOperationCodeDTO();
            result.setId(operationCode.getId());
            result.setCode(operationCode.getCode());
            result.setName(operationCode.getName());
            result.setCommissionRate(operationCode.getCommissionRate());

            return result;
        }
        return null;
    }

}
