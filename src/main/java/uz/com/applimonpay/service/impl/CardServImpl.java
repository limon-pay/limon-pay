package uz.com.applimonpay.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.com.applimonpay.common.ResponseData;
import uz.com.applimonpay.dto.card.*;
import uz.com.applimonpay.dto.transaction.ReqTransactionParam;
import uz.com.applimonpay.entity.Card;
import uz.com.applimonpay.entity.User;
import uz.com.applimonpay.exception.CardNotFoundException;
import uz.com.applimonpay.exception.PanShouldNotBeNullException;
import uz.com.applimonpay.helper.UserSession;
import uz.com.applimonpay.helper.Utils;
import uz.com.applimonpay.mapper.CardMap;
import uz.com.applimonpay.repo.CardRepo;
import uz.com.applimonpay.service.CardServ;
import uz.com.applimonpay.service.UserServ;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@Slf4j
@RequiredArgsConstructor
public class CardServImpl implements CardServ {

    private final CardRepo repo;
    private final UserServ userService;
    private final CardMap mapper;
    private final UserSession session;

    @Override
    public ResponseEntity<ResponseData<List<CardDTO>>> getAll() {
        List<Card> list = repo.findAllByUserId(session.getUser().getId());
        if (Utils.isEmpty(list)) {
            log.warn("Kartalar ro'yxati topilmadi!");
            return ResponseData.notFoundData("Cards are not found !!!");
        }
        List<CardDTO> dtoList = new ArrayList<>();
        list.forEach(cards -> dtoList.add(mapper.toDto(cards)));
        log.info("Barcha kartalar ro'yxati olindi");
        return ResponseData.success200(dtoList);
    }

    @Override
    public ResponseEntity<ResponseData<CardDTO>> get(UUID uuid) {
        Optional<Card> card = repo.findByUuid(uuid);
        if (Utils.isEmpty(card)) {
            log.error("Card uuid, {} bo'yicha ma'lumot topilmadi", uuid);
            ResponseData.notFoundData("Card not found !!!");
        }
        log.info("Card uuid, {} bo'yicha ma'lumot olindi", uuid);
        return ResponseData.success200(mapper.toDto(card.get()));
    }

    @Override
    public UserCardDTO getUserCards(UUID uuid) throws CardNotFoundException {
        Optional<Card> optional = repo.findByUuid(uuid);
        if (optional.isEmpty()) {
            log.error("Card uuid, {} bo'yicha ma'lumot topilmadi", uuid);
            throw new CardNotFoundException("Card not found !!!");
        }
        Card card = optional.get();
        log.info("Card uuid, {} bo'yicha ma'lumot olindi", uuid);
        return mapper.toEntity(card);
    }

    @Override
    public ResponseEntity<ResponseData<CardDTO>> add(CardCrDTO dto) throws Exception {

        User user = userService.findByUuid(UUID.fromString(dto.getUserUuid()));

        String endPan = dto.getPan().substring(4, 16);

        String pan = dto.getType().getCode() + endPan;

        String start = pan.substring(0, 6);
        String end = pan.substring(12, 16);

        final String maskedPan = start + "******" + end;

        Optional<Card> cardOptional = repo.findByPan(pan);
        if (cardOptional.isPresent()) {
            log.info("Card pan, {} oldin ro'yxatdan o'tgan", pan);
            return ResponseData.alreadyExists("Card pan is already registered!");
        }

        Card card = new Card();
        card.setPan(pan);
        card.setExpiry(dto.getExpiry());
        card.setMaskedPan(maskedPan);
        card.setCardHolderName(dto.getCardHolderName());
        card.setType(dto.getType());
        card.setCurrency(dto.getCurrency());
        card.setBalance(0L);
        card.setPhone(dto.getPhone());
        card.setUserId(user.getId());
        card.setCardToken("CT_" + System.currentTimeMillis());

        repo.save(card);
        log.info("Yangi card - {} saqlandi", card.getMaskedPan());
        return ResponseData.success201(mapper.toDto(card));
    }

    @CachePut(cacheNames = {"CardServ.edit"}, key = "#dto.uuid")
    @Override
    public ResponseEntity<ResponseData<CardDTO>> edit(CardUpDTO dto) {

       /* try {
            Thread.sleep(5 * 1000); // 5 seconds
        } catch (InterruptedException e) {
            // ignored
        }*/

        Optional<Card> optional = repo.findByUuid(dto.getUuid());
        if (optional.isEmpty()) {
            log.error("Card uuid, {} bo'yicha ma'lumot topilmadi", dto.getUuid());
            return ResponseData.notFoundData("Card not found !!!");
        }
        Card card = optional.get();
        if (!card.isActive()) {
            log.error("Card uuid, {} bo'yicha faol emas!", dto.getUuid());
            return ResponseData.inActive("This card is not active !!!");
        }
        card = mapper.toEntity(card, dto);
        repo.save(card);
        log.info("Card {} - ma'lumotlari yangilandi!", card.getMaskedPan());
        return ResponseData.success202(mapper.toDto(card));
    }

    @CacheEvict(cacheNames = {"CardServ.delete"}, key = "#dto.uuid")
    @Override
    public ResponseEntity<ResponseData<CardDTO>> delete(CardDelDTO dto) {

        try {
            Thread.sleep(5 * 1000); // 5 seconds
        } catch (InterruptedException e) {
            // ignored
        }

        Optional<Card> optional = repo.findByUuid(dto.getUuid());
        Card card = optional.get();
        if (card.isDeleted()) {
            log.error("Card uuid, {} bo'yicha oldin o'chirilgan", dto.getUuid());
            return ResponseData.isDeleted("This user was previously disabled !!!");
        } else if (!card.getPan().equals(dto.getPan())) {
            log.error("Card pan, {} bo'yicha pan mos emas!", dto.getPan());
            return ResponseData.errorStatus("Pan is incorrect", HttpStatus.NOT_FOUND);
        } else if (!card.getPhone().equals(dto.getPhone())) {
            log.error("Card phone, {} bo'yicha phone mos emas!", dto.getPhone());
            return ResponseData.errorStatus("Phone is incorrect", HttpStatus.NOT_FOUND);
        } else if (!card.getUuid().equals(dto.getUuid())) {
            log.error("Card uuid, {} bo'yicha ma'lumot topilmadi", dto.getUuid());
            return ResponseData.notFoundData("Card not found !!!");
        }
        card.setDeleted(true);
        card.setActive(false);
        card.setPan(card.getPan() + "_isDel_" + System.currentTimeMillis());
        repo.save(card);
        log.info("Card uuid, {} bo'yicha o'chirildi!", dto.getUuid());
        return ResponseData.success200(mapper.toDto(card));
    }

    @Override
    public Card findByCardToken(String cardToken) throws CardNotFoundException {
        Optional<Card> optional = repo.findByCardToken(cardToken);
        if (optional.isEmpty()) {
            log.error("Card cardToken, {} bo'yicha ma'lumot topilmadi", cardToken);
            throw new CardNotFoundException("Card not found !!!");
        }
        Card card = optional.get();
        return card;
    }

    public Card findByUuid(UUID uuid) throws CardNotFoundException {
        Optional<Card> cardOptional = repo.findByUuid(uuid);
        if (cardOptional.isEmpty()) {
            log.error("Card uuid, {} bo'yicha ma'lumot topilmadi", uuid);
            throw new CardNotFoundException("Card is not found");
        }
        return cardOptional.get();
    }

    @Override
    public Card findByPan(String pan) throws CardNotFoundException, PanShouldNotBeNullException {

        if (Utils.isEmpty(pan)) {
            log.warn("Pan null bo'lmasligi kerak!");
            throw new PanShouldNotBeNullException("Pan should not be null !!!");
        }

        Optional<Card> cardOptional = repo.findByPan(pan);
        if (cardOptional.isEmpty()) {
            log.error("Card pan, {} bo'yicha ma'lumot topilmadi", pan);
            throw new CardNotFoundException("Card is not found");
        }
        return cardOptional.get();

    }

    @Override
    public UserCardDTO getCard(ReqTransactionParam param) throws CardNotFoundException {

        UserCardDTO senderCard = this.getUserCards(param.getUuid());
        UserCardDTO receiverCard = this.getUserCards(param.getUuid());

        if (Utils.isPresent(param.getUuid())) {
            // todo shu yerda sender card bazadan olib chqiladi
            return senderCard;
        } else {
            // todo receiver card processing markazdan olib kelinadi
            String receiverPan = receiverCard.getPan();
            if (Utils.isPresent(receiverPan)) {
                return receiverCard;
            }
            throw new CardNotFoundException("Receiver card not found !!!");
        }
    }
}
