package uz.com.applimonpay.service;

import uz.com.applimonpay.dto.transaction.*;

public interface TransactionServ {
    ResTransactionHoldDTO hold(ReqTransactionHoldDTO dto) throws Exception;

    ResTransactionConfirmDTO confirm(ReqTransactionConfirmDTO dto) throws Exception;

    ResTransactionHistoryDTO history(ReqTransactionHistoryDTO dto) throws Exception;

}
