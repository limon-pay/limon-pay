package uz.com.applimonpay.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import uz.com.applimonpay.base.BaseEntity;
import uz.com.applimonpay.enums.CurrencyType;
import uz.com.applimonpay.enums.TransactionStatus;
import uz.com.applimonpay.enums.TransactionType;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "transactions")
public class Transaction extends BaseEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "operation_id", referencedColumnName = "id", insertable = false, updatable = false)
    private Operation operation;
    @Column(name = "operation_id")
    private Long operationId;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "type")
    private TransactionType type;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "status")
    private TransactionStatus status;

    @Column(name = "request_id")
    private String requestId;

    @Column(name = "response_id")
    private String responseId;

    @Column(name = "error_message")
    private String errorMessage;

    @Column(name = "amount")
    private Long amount;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "currency")
    private CurrencyType currency;

}
