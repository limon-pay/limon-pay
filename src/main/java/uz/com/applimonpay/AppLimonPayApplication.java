package uz.com.applimonpay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableCaching
@SpringBootApplication
public class AppLimonPayApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppLimonPayApplication.class, args);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

   /* @Bean
    public CommandLineRunner run(UserServ serv) {
        return args -> {
            // create role
            serv.saveRole(new Role(null, "ROLE_SUPER_ADMIN"));
            serv.saveRole(new Role(null, "ROLE_ADMIN"));
            serv.saveRole(new Role(null, "ROLE_USER"));

            //CREATE USER
            serv.saveUser(new User("SUPER", "ADMIN", "super-admin", "912345678", "1111", new ArrayList<>()));
            serv.saveUser(new User("ADMIN", "ADMIN", "admin", "991234567", "2222", new ArrayList<>()));
            serv.saveUser(new User("USER", "USER", "user", "901112233", "3333", new ArrayList<>()));

            // attach
            serv.attachRoleToUser("912345678", "ROLE_SUPER_ADMIN");
            serv.attachRoleToUser("991234567", "ROLE_ADMIN");
            serv.attachRoleToUser("901112233", "ROLE_USER");
        };
    }*/

}
